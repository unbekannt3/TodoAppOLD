import { TestBed, inject } from '@angular/core/testing';

import { PriorityRestDataService } from './priority-rest-data.service';

describe('PriorityRestDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PriorityRestDataService]
    });
  });

  it('should be created', inject([PriorityRestDataService], (service: PriorityRestDataService) => {
    expect(service).toBeTruthy();
  }));
});
