import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from '../models/todo';
import { MessageBoxService } from './message-box.service';

@Injectable()
export class TodoRestDataService {

  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  private todoDataCache: Todo[];
  private rotateActive = false;

  constructor(private http: HttpClient, private messageBoxService: MessageBoxService) {
    this.syncData();
    if (localStorage.getItem('delEnabled') === null) {
      localStorage.setItem('delEnabled', 'true');
    }
    if (localStorage.getItem('searchEnabled') === null) {
      localStorage.setItem('searchEnabled', 'true');
    }
    if (localStorage.getItem('todoOrdering') === null) {
      localStorage.setItem('todoOrdering', 'false');
    }
  }

  private syncData() {
    this.runSyncIndicator();
    this.http.get('api/todos').subscribe(data => {
      this.todoDataCache = <Todo[]>data;
    }, err => {
      this.messageBoxService.display('Ladefehler', 'Todos konnten nicht geladen werden - Fehler: ' + err.status, 'error');
      console.log(err);
    });
  }


  get todoList(): Todo[] {
    return this.todoDataCache;
  }

  toggle(_todo: Todo) {
    _todo.done = !_todo.done;
    const body = new URLSearchParams();
    body.set('done', _todo.done ? '1' : '0');
    this.http.patch(
      `api/todos/${_todo.id}`,
      body.toString(),
      this.options).subscribe(
      () => this.syncData(),
      (err) => {
        // reverse change
        this.syncData();
        this.messageBoxService.display('Fehler beim Speichern', 'Fehlercode: ' + err.status, 'error');
        console.log(err);
      }
      );
  }

  add(_todo: Todo) {
    const body = new URLSearchParams();
    body.set('label', _todo.label);
    body.set('done', String(_todo.done));
    body.set('categoryID', String(_todo.category.id));
    if (_todo.dueDate) {
      body.set('dueDate', String(_todo.dueDate));
    }
    if (_todo.priority) {
      body.set('priorityID', String(_todo.priority.id));
    }
    console.log('body: ' + body.toString());
    this.http.post(`api/todos`, body.toString(), this.options).subscribe(() => {
      this.syncData();
      this.todoDataCache.push(_todo);
    }, (err) => {
      // reverse change
      this.syncData();
      this.messageBoxService.display('Fehler beim Speichern', 'Fehlercode: ' + err.status, 'error');
      console.log(err);
    });
  }

  delete(id: number) {
    this.todoDataCache = this.todoDataCache.filter(t => t.id !== id);
    this.http.delete(
      `api/todos/` + id).subscribe(
      () => this.syncData(),
      err => {
        console.log(err);
        this.syncData();
        this.messageBoxService.display('Fehler beim Löschen', 'Fehlercode: ' + err.status, 'error');
      });
  }

  // Alle Todos löschen
  deleteAll(alert?: boolean) {
    if (alert === null) {
      alert = true;
    }

    // DB & Array leeren
    this.todoDataCache.forEach(t => {
      this.http.delete(
        `api/todos/` + t.id).subscribe(() => this.syncData(), err => {
          console.log(err);
          this.syncData();
          this.messageBoxService.display('Fehler beim Löschen', 'Todo: "' + t.label + '", Fehlercode: ' + err.status, 'error');
        });
    });
    console.log(JSON.stringify(this.todoDataCache));

    if (alert) {
      this.messageBoxService.display('Gelöscht!', 'All deine Todos wurden beerdigt', 'warning');
    }

  }

  get todosLeer(): boolean {

    if (this.todoDataCache.length < 1) {
      return true;
    } else {
      return false;
    }
  }

  synchronizeWithDB() {
    this.syncData();
  }

  runSyncIndicator() {
    this.rotateActive = true;
    setTimeout(function () {
      this.rotateActive = false;
    }.bind(this), 1000);
  }

  get rotateStatus() {
    return this.rotateActive;
  }

  get nextID(): number {
    return this.todoList.length++;
  }

}
