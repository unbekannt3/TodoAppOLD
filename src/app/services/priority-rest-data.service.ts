import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageBoxService } from './message-box.service';
import { Priority } from '../models/priority';

@Injectable()
export class PriorityRestDataService {

  currentPriority: number;

  private priorityDataCache: Priority[] = [];

  private refresh() {
    this.http.get('api/priorities').subscribe(data => {
      this.priorityDataCache = <Priority[]>data;
    }, err => {
      console.log(err);
      this.MessageBoxService.display('Beim Laden der Kategorien ist ein Fehler aufgetreten: ' + err.status, 'danger');
    });
  }


  get priorities(): Priority[] {
    return this.priorityDataCache;
  }

  constructor(private http: HttpClient, private MessageBoxService: MessageBoxService) {
    this.refresh();
  }

}
