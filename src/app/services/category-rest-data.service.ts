import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageBoxService } from './message-box.service';
import { Category } from '../models/category';

@Injectable()
export class CategoryRestDataService {

  currentCategory: number;

  private categoryDataCache: Category[] = [];

  private refresh() {
    this.http.get('api/categories').subscribe(data => {
      this.categoryDataCache = <Category[]>data;
    }, err => {
      console.log(err);
      this.messageBoxService.display('Beim Laden der Kategorien ist ein Fehler aufgetreten: ' + err.status, 'danger');
    });
  }


  get categories(): Category[] {
    return this.categoryDataCache;
  }

  constructor(private http: HttpClient, private messageBoxService: MessageBoxService) {
    this.refresh();
  }

}
