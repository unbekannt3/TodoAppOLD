import { Injectable } from '@angular/core';
import { Category } from '../models/category';

@Injectable()
export class TodoCategoryService {

  public categories: Category[] = [];

  constructor() {

    this.categories.push(new Category(1, 'Haus'));
    this.categories.push(new Category(2, 'Garten'));
    this.categories.push(new Category(3, 'Spiele'));
    this.categories.push(new Category(4, 'Finanzen'));

  }

}
