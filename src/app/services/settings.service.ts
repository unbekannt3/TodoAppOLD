import { CategoryRestDataService } from './category-rest-data.service';
import { Injectable } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

import { ExcelService } from './excel-service.service';
import { MessageBoxService } from './message-box.service';
import { TodoCategoryService } from './todo-category.service';
import { TodoDataService } from './todo-data.service';
import { TodoPriorityService } from './todo-priority.service';
import { TodoRestDataService } from './todo-rest-data.service';
import { PriorityRestDataService } from './priority-rest-data.service';

declare var swal: any;

@Injectable()
export class SettingsService {

  private version = '1.0.3';
  private rotateActive = false;

  constructor(
    private todoDataService: TodoRestDataService,
    private todoCategoryService: CategoryRestDataService,
    private todoPriorityService: PriorityRestDataService,
    private messageBoxService: MessageBoxService,
    private excelService: ExcelService,
    private alertService: SweetAlertService
  ) { }



  // Beispieldaten laden
  loadExampleData() {
    // Array leeren
    if (this.todoDataService.todoList.length != null) {
      this.todoDataService.todoList.length = 0;
    }
    // Mit Beispieldaten befüllen
    JSON.parse(localStorage.getItem('egTodos')).forEach(todo => {
      this.todoDataService.add(todo);
    });
    this.messageBoxService.display('Beispieldaten erfolgreich geladen', 'success');

    // LocalStorage Updaten
    this.updateLocalStorageArray();
  }

  // Löschbutton
  toggleDeleteButton() {
    if (localStorage.getItem('delEnabled') === 'false' || null) {
      localStorage.setItem('delEnabled', 'true');
    } else {
      localStorage.setItem('delEnabled', 'false');
    }
  }

  // Suchfeld
  toggleSearchField() {
    if (localStorage.getItem('searchEnabled') === 'false' || null) {
      localStorage.setItem('searchEnabled', 'true');
    } else {
      localStorage.setItem('searchEnabled', 'false');
    }
  }

  // Todo List Ordering
  toggleTodoOrdering() {
    if (localStorage.getItem('todoOrdering') === 'false' || null) {
      localStorage.setItem('todoOrdering', 'true');
    } else {
      localStorage.setItem('todoOrdering', 'false');
    }
  }

  // Alle Todos löschen
  deleteAll(alert?: boolean) {
    if (alert === null) {
      alert = true;
    }
    this.todoDataService.deleteAll();
  }

  // Export zu Excel Tabelle
  exportData() {
    if (this.todoDataService.todoList.length >= 1) {
      this.alertService.question({
        text: 'Alle Todos als Excel Mappe exportieren?',
        showCancelButton: true,
        confirmButtonText: 'Download',
        cancelButtonText: 'Abbrechen',
        confirmButtonColor: '#4caf50',
        cancelButtonColor: '#e51c23',
      })
        .then(() => {
          this.excelService.exportAsExcelFile(this.todoDataService.todoList, 'TodoApp');
          this.alertService.success({
            title: 'Todos Exportiert',
            text: 'Dateiname: "TodoApp_export_' + this.excelService.getExportDate + '.xlsx"'
          });
        })
        .catch(() => {
          this.alertService.error({
            text: 'Exportvorgang abgebrochen'
          });
        });
    } else {
      this.messageBoxService.display('Ohne Todos musst du auch nichts Exportieren...', 'danger');
    }
  }

  _loadExampleTodos(): boolean {

    let overwrite = false;

    swal({
      title: 'Todos überschreiben',
      text: 'Sollen deine vorhandenen Todos überschrieben werden?',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Überschreiben',
      cancelButtonText: 'Zu existierenden hinzufügen'
    }).then(function () {
      swal({
        title: '',
        text: 'Deine Todos wurden durch die Beispiel Todos ersetzt',
        type: 'success',
        timer: 3000
      });
      overwrite = true;
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: '',
          text: 'Deine Todos wurden vor ihrem Grab bewart.',
          type: 'success',
          timer: 3000
        });
        overwrite = false;
      }
    });

    return overwrite;
  }

  // Array in localStorage Speichern
  updateLocalStorageArray() {
    localStorage.setItem('todos', JSON.stringify(this.todoDataService.todoList));
    console.log('[DEBUG] todos in localStorage updated');
  }

  get delEnabled(): boolean {
    if (localStorage.getItem('delEnabled') === 'true') {
      return true;
    } else {
      return false;
    }

  }

  get searchEnabled(): boolean {
    if (localStorage.getItem('searchEnabled') === 'true') {
      return true;
    } else {
      return false;
    }

  }

  get listSortEnabled(): boolean {
    if (localStorage.getItem('todoOrdering') === 'true') {
      return true;
    } else {
      return false;
    }
  }

  get rotateStatus(): boolean {
    return this.rotateActive;
  }

  get appVersion() {

    return this.version;
  }

}
