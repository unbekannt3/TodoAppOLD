import { Injectable } from '@angular/core';

@Injectable()
export class MessageBoxService {

  private boxtext: string;
  private typ: string;
  private show = false;

  constructor(
  ) { }

  // timeout(): void {
  //   this.show = true;
  //   setTimeout(function () {
  //     this.show = false;
  //   }.bind(this), 3000);
  // }

  public options = {
    timeOut: 3000,
    progressBar: true,
    maxOpened: 8
  };

  display(title: string, message: string, typ?: string) {
    // this.timeout();
    if (typ === null || '') {
      typ = 'info';
    }
    // if (typ === 'success') {
    //   this.alertService.success(
    //     message,
    //     title,
    //     this.options
    //   );
    // }
    // if (typ === 'error') {
    //   this.alertService.error(
    //     message,
    //     title,
    //     this.options
    //   );
    // }
    // if (typ === 'warn') {
    //   this.alertService.warning(
    //     message,
    //     title,
    //     this.options
    //   );
    // }
    // if (typ === 'info') {
    //   this.alertService.info(
    //     message,
    //     title,
    //     this.options
    //   );
    // }
    this.typ = typ;
    return this.boxtext = message;
  }

}
