import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Todo } from '../models/todo';
import { MessageBoxService } from './message-box.service';
import { TodoCategoryService } from './todo-category.service';
import { TodoPriorityService } from './todo-priority.service';

@Injectable()
export class TodoDataService {

  public todos: Todo[] = [];
  private exampleTodos: Todo[] = [];

  constructor(
    private todoCategoryService: TodoCategoryService,
    private messageBoxService: MessageBoxService,
    private todoPriorityService: TodoPriorityService,
    private router: Router
  ) {

    // Beispieldaten
    this.exampleTodos.push(new Todo(1, 'Staubsaugen', new Date(),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(2, 'Rasen mähen', new Date(),
      this.todoCategoryService.categories[1], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(3, 'Spiel kaufen', new Date(2019, 8, 26),
      this.todoCategoryService.categories[2], this.todoPriorityService.priorities[1]));
    this.exampleTodos.push(new Todo(4, 'Abo kündigen', new Date(2017, 12, 14),
      this.todoCategoryService.categories[3], this.todoPriorityService.priorities[2], true));
    this.exampleTodos.push(new Todo(5, 'Vorräte auffüllen', new Date(2018, 12, 22),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[2]));
    this.exampleTodos.push(new Todo(6, 'Unnütze Bäume fällen', new Date(2018, 8, 14),
      this.todoCategoryService.categories[1], this.todoPriorityService.priorities[2]));
    this.exampleTodos.push(new Todo(7, 'PC Aufrüsten', new Date(),
      this.todoCategoryService.categories[2], this.todoPriorityService.priorities[0]));
    this.exampleTodos.push(new Todo(8, 'Bank wechseln', new Date(2018, 2, 9),
      this.todoCategoryService.categories[3], this.todoPriorityService.priorities[1], true));
    this.exampleTodos.push(new Todo(9, 'Müll ausleeren', new Date(2017, 9, 8),
      this.todoCategoryService.categories[0], this.todoPriorityService.priorities[1]));

    // Properties in LocalStorage speichern, wenn nicht vorhanden oder ungleich
    if (JSON.parse(localStorage.getItem('todos')) === null) {
      localStorage.setItem('todos', JSON.stringify(this.todos));
    }

    if (localStorage.getItem('egTodos') === null || JSON.parse(localStorage.getItem('egTodos')) !== this.exampleTodos) {
      localStorage.setItem('egTodos', JSON.stringify(this.exampleTodos));
    }
    if (localStorage.getItem('delEnabled') === null) {
      localStorage.setItem('delEnabled', 'true');
    }
    if (localStorage.getItem('searchEnabled') === null) {
      localStorage.setItem('searchEnabled', 'true');
    }
    if (localStorage.getItem('todoOrdering') === null) {
      localStorage.setItem('todoOrdering', 'false');
    }

    // Todo Array aus LocalStorage in Property parsen
    this.todos = JSON.parse(localStorage.getItem('todos'));
  }

  // Todo status ändern
  toggle(_todo: Todo) {
    _todo.done = !_todo.done;

    // LocalStorage Updaten
    this.updateLocalStorageArray();
  }

  // Einzelnes Todo hinzufügen
  add(_todo: Todo, alert?: boolean) {
    if (alert === null) {
      alert = true;
    }

    if (_todo.label && _todo.category && _todo.priority) {

      this.todos.push(new Todo(this.todos.length + 1, _todo.label, _todo.dueDate, _todo.category, _todo.priority, _todo.done));
      if (alert) {
        this.messageBoxService.display('Todo "' + _todo.label + '" erfolgreich hinzugefügt', 'success');
      }
      _todo.label = '';
      this.router.navigateByUrl('todos');

      // LocalStorage Updaten
      this.updateLocalStorageArray();

    } else {
      this.messageBoxService.display('Bitte die rot markierten Felder ausfüllen', 'danger');
    }
  }

  // Einzelnes Todo löschen
  delete(_todo: Todo) {

    this.todos = this.todos.filter(t => t.id !== _todo.id);
    for (let i = 0; i < this.todos.length; i++) {
      this.todos[i].id = i + 1;
    }
    this.messageBoxService.display('Todo "' + _todo.label + '" erfolgreich gelöscht', 'warning');

    // LocalStorage Updaten
    this.updateLocalStorageArray();
  }

  // Todo Array leeren
  clearArray(alert?: boolean) {
    if (alert === null) {
      alert = true;
    }

    // Array leeren
    this.todos.length = 0;
    if (alert) {
      this.messageBoxService.display('Deine Todos wurden beerdigt', 'warning');
    }

    // LocalStorage Updaten
    this.updateLocalStorageArray();

  }

  // Array in localStorage Speichern
  updateLocalStorageArray() {
    localStorage.setItem('todos', JSON.stringify(this.todos));
    console.log('[DEBUG] todos in localStorage updated');
  }

  // Alert anzeigen wenn keine Todos vorhanden
  leer(): boolean {

    let leer;
    if (this.todos.length < 1) {
      leer = true;
    } else {
      leer = false;
    }
    return leer;
  }

  get nextID(): number {
    return this.todos.length++;
  }

}
