import { TestBed, inject } from '@angular/core/testing';

import { TodoRestDataService } from './todo-rest-data.service';

describe('TodoRestDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoRestDataService]
    });
  });

  it('should be created', inject([TodoRestDataService], (service: TodoRestDataService) => {
    expect(service).toBeTruthy();
  }));
});
