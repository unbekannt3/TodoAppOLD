import { TestBed, inject } from '@angular/core/testing';

import { CategoryRestDataService } from './category-rest-data.service';

describe('CategoryRestDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoryRestDataService]
    });
  });

  it('should be created', inject([CategoryRestDataService], (service: CategoryRestDataService) => {
    expect(service).toBeTruthy();
  }));
});
