import { Component, OnInit } from '@angular/core';

import { Todo } from '../../models/todo';
import { SettingsService } from '../../services/settings.service';
import { TodoDataService } from '../../services/todo-data.service';
import { TodoCategoryService } from '../../services/todo-category.service';
import { TodoPriorityService } from '../../services/todo-priority.service';
import { CategoryRestDataService } from '../../services/category-rest-data.service';
import { PriorityRestDataService } from '../../services/priority-rest-data.service';
import { TodoRestDataService } from '../../services/todo-rest-data.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  searchString: string;

  constructor(
    private todoDataService: TodoRestDataService,
    private todoCategoryService: CategoryRestDataService,
    private todoPriorityService: PriorityRestDataService,
    private settingsService: SettingsService
  ) { }

  toggle(todo: Todo) {

    this.todoDataService.toggle(todo);
  }

  add(todo: Todo) {

    this.todoDataService.add(todo);
  }

  delete(id: number) {

    this.todoDataService.delete(id);
  }

  get todos() {
    return this.todoDataService.todoList;
  }

  get leer(): boolean {

    return this.todoDataService.todosLeer;
  }

  get nextID(): number {

    return this.todoDataService.nextID;
  }

  get filter(): string {
    return this.searchString;
  }

  get delEnabled() {
    return this.settingsService.delEnabled;
  }

  get searchEnabled() {
    return this.settingsService.searchEnabled;
  }

  ngOnInit() {
  }

}
