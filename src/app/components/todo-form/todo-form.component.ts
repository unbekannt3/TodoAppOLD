import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';

import { Todo } from '../../models/todo';
import { TodoRestDataService } from '../../services/todo-rest-data.service';
import { CategoryRestDataService } from '../../services/category-rest-data.service';
import { PriorityRestDataService } from '../../services/priority-rest-data.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {

  labelblur = false;
  catblur = false;
  prioblur = false;
  datepickObj: Object;

  constructor(
    private todoDataService: TodoRestDataService,
    private categoryService: CategoryRestDataService,
    private todoPriorityService: PriorityRestDataService,
    private router: Router
  ) { }

  todo: Todo = new Todo(0, '');

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy',
  };

  add() {
    if (this.datepickObj != null) {
      const dateStr = `${this.datepickObj['date']['year']}/${this.datepickObj['date']['month']}/${this.datepickObj['date']['day']}`;
      this.todo.dueDate = new Date(dateStr);
    }
    this.todoDataService.add(this.todo);
    this.catblur = true;
    this.labelblur = true;
    this.prioblur = true;
  }

  setPriority(_priority: number) {
    if (_priority === 1) {
      this.todo.priority = this.todoPriorityService.priorities[0];
    }
    if (_priority === 2) {
      this.todo.priority = this.todoPriorityService.priorities[1];
    }
    if (_priority === 3) {
      this.todo.priority = this.todoPriorityService.priorities[2];
    }
  }

  delete(id: number) {
    this.todoDataService.delete(id);
  }

  checklab(_todo: Todo): boolean {
    if (this.labelblur && !_todo.label) {
      return true;
    } else {
      return false;
    }
  }

  checkcat(_todo: Todo): boolean {
    if (this.catblur && !_todo.category) {
      return true;
    } else {
      return false;
    }
  }

  checkprio(_todo: Todo): boolean {
    if (this.prioblur && !_todo.priority) {
      return true;
    } else {
      return false;
    }
  }

  get categories() {
    return this.categoryService.categories;
  }

  get priorities() {
    return this.todoPriorityService.priorities;
  }

  ngOnInit() {
  }

}
