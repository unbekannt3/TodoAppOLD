import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-priority-chooser',
  templateUrl: './priority-chooser.component.html',
  styleUrls: ['./priority-chooser.component.scss']
})
export class PriorityChooserComponent implements OnInit {

  @Input() currentlySelectedPriority: number;

  @Output() priorityUpdated = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  select(priority: number) {
    if (this.currentlySelectedPriority !== priority) {
      this.priorityUpdated.emit(priority);
    } else {
      this.priorityUpdated.emit(undefined);
    }
  }

}
