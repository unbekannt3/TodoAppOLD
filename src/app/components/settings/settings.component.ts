import { Component, OnInit } from '@angular/core';

import { SettingsService } from '../../services/settings.service';
import { TodoDataService } from '../../services/todo-data.service';

declare var swal: any;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(
    private todoDataService: TodoDataService,
    private settingsService: SettingsService
  ) { }

  // Toggle Delete
  toggleDelete() {
    this.settingsService.toggleDeleteButton();
  }

  // Toggle Search Field
  toggleSearchField() {
    this.settingsService.toggleSearchField();
  }

  // Toggle Todo Ordering
  toggleTodoOrdering() {
    this.settingsService.toggleTodoOrdering();
  }

  // Load Example Todos
  loadExampleData() {

    this.settingsService.loadExampleData();
  }

  // Export as XLSX File
  exportData() {
    this.settingsService.exportData();
  }

  // Remove all Todos
  deleteAll() {
    this.settingsService.deleteAll(true);
  }

  // Question for Todo clearing
  _clearArray(): boolean {

    let del = false;

    swal({
      title: 'Bist du dir sicher?',
      text: 'Deine Todos sind dann für immer Verloren',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Lösch es!',
      cancelButtonText: 'Ich überleg\'s mir nochmal'
    }).then(function () {
      swal({
        title: 'Gelöscht!',
        text: 'Deine Todos wurden beerdigt.',
        type: 'success',
        timer: 3000
      });
      del = true;
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal({
          title: 'Ok',
          text: 'Deine Todos sind gerettet.',
          type: 'error',
          timer: 3000
        });
        del = false;
      }
    });

    return del;
  }

  _clear() {
    this.settingsService.deleteAll();
  }

  get delEnabled() {

    return this.settingsService.delEnabled;
  }

  get searchEnabled() {

    return this.settingsService.searchEnabled;
  }

  get todoOrderingEnabled() {

    return this.settingsService.listSortEnabled;
  }

  get getVersion() {

    return this.settingsService.appVersion;
  }

  ngOnInit() {
  }

}
