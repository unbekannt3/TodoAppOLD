import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule, Routes } from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
import { MyDatePickerModule } from 'mydatepicker/dist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PriorityChooserComponent } from './components/priority-chooser/priority-chooser.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TodoFormComponent } from './components/todo-form/todo-form.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoStatisticsComponent } from './components/todo-statistics/todo-statistics.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { TodoFilterPipe } from './pipes/todo-filter.pipe';
import { TodoListOrderingPipe } from './pipes/todo-list-ordering.pipe';
import { TodoDataService } from './services/todo-data.service';
import { TodoCategoryService } from './services/todo-category.service';
import { TodoPriorityService } from './services/todo-priority.service';
import { MessageBoxService } from './services/message-box.service';
import { SettingsService } from './services/settings.service';
import { ExcelService } from './services/excel-service.service';
import { TodoRestDataService } from './services/todo-rest-data.service';
import { CategoryRestDataService } from './services/category-rest-data.service';
import { PriorityRestDataService } from './services/priority-rest-data.service';

const appRoutes: Routes = [
  {
    path: 'todos',
    component: TodoListComponent,
    data: { title: '' }
  },
  {
    path: 'add',
    component: TodoFormComponent,
    data: { title: 'Neues Todo' }
  },
  {
    path: 'settings',
    component: SettingsComponent,
    data: { title: 'Einstellungen' }
  },
  {
    path: '',
    redirectTo: '/todos',
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoFormComponent,
    TodoStatisticsComponent,
    NavbarComponent,
    TodoFilterPipe,
    SettingsComponent,
    TodoListOrderingPipe,
    PriorityChooserComponent,
    SideMenuComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    MyDatePickerModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
  ],
  providers: [
    { provide: TodoDataService, useClass: TodoRestDataService },
    { provide: TodoCategoryService, useClass: CategoryRestDataService },
    { provide: TodoPriorityService, useClass: PriorityRestDataService },
    TodoRestDataService,
    CategoryRestDataService,
    PriorityRestDataService,
    MessageBoxService,
    SettingsService,
    ExcelService,
    SweetAlertService,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
